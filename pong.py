
import turtle
import winsound

window = turtle.Screen()

window.title("Pong @Cybertutoriales")
window.bgcolor("green")
window.setup(width=800, height=600)
window.tracer(0)

## Variables para el Marcador
marcador_a = 0
marcador_b = 0

# Paleta A
paleta_a = turtle.Turtle()
paleta_a.speed(0)
paleta_a.shape("square")
paleta_a.color("yellow")
paleta_a.shapesize(stretch_wid=5, stretch_len=1) 
paleta_a.penup()
paleta_a.goto(-350, 0) #tupla de las coordenadas (x,y)

# Paleta B
paleta_b = turtle.Turtle()
paleta_b.speed(0)  
paleta_b.shape("square")
paleta_b.color("red")
paleta_b.shapesize(stretch_wid=5, stretch_len=1) 
paleta_b.penup()
paleta_b.goto(350, 0)

# Pelota
pelota = turtle.Turtle()
pelota.speed(2)  
pelota.shape("square")
pelota.color("yellow")
pelota.penup()
pelota.goto(0, 0)
# Para hacer mover la pelota
pelota.dx = 0.4
pelota.dy = 0.4
# Pen
pen = turtle.Turtle()
pen.speed(0)
pen.color("white")
pen.penup()
pen.hideturtle()
pen.goto(0, 260)
pen.write("Jugador A : 0  Jugador B : 0", align="center", font=("Courier", 24, "normal"))

## Moviendo las paletas con el teclado
# PALETA A Hacia Arriba
def paleta_a_up():
    y = paleta_a.ycor() # método de la clase Turtle
    y += 20
    paleta_a.sety(y) #forma en que actualizamos el valor de la cordenada y
    # tope en borde inferior de la ventana
    if paleta_a.ycor() > 250:
        paleta_a.sety(250)

# Keyboard binding / Enlazando el evento de teclado

# PALETA A Hacia abajo
def paleta_a_down():
    y = paleta_a.ycor() # método de la clase Turtle
    y -= 20 ## *en lugar de sumar restamos
    paleta_a.sety(y)
    # tope en borde inferior de la ventana
    if paleta_a.ycor() < -250:
        paleta_a.sety(-250)

# PALETA A Hacia Arriba
def paleta_b_up():
    y = paleta_b.ycor() # método de la clase Turtle
    y += 20
    paleta_b.sety(y) #forma en que actualizamos el valor de la cordenada y
    # tope en borde inferior de la ventana
    if paleta_b.ycor() > 250:
        paleta_b.sety(250)

# Keyboard binding / Enlace de evento del teclado

# PALETA A Hacia abajo
def paleta_b_down():
    y = paleta_b.ycor() # método de la clase Turtle
    y -= 20 ## *en lugar de sumar restamos
    paleta_b.sety(y)
    # tope en borde inferior de la ventana
    if paleta_b.ycor() < -250: 
        paleta_b.sety(-250)    

window.listen() 
window.onkeypress(paleta_a_up, "w") 
window.onkeypress(paleta_a_down, "s") 
window.onkeypress(paleta_b_up, "Up") 
window.onkeypress(paleta_b_down, "Down")    

# Main Game Loop
while True:
    window.update()

    #Move the ball
    pelota.setx(pelota.xcor() + pelota.dx)
    pelota.sety(pelota.ycor() + pelota.dy)

    # Revisión de bordes
    if pelota.ycor() > 290:
        pelota.sety(290)
        pelota.dy *= -1
        # winsound.PlaySound("bounce.wav", winsound.SND_ASYNC)
    
    elif pelota.ycor() < -290:
        pelota.sety(-290)
        pelota.dy *= -1
        winsound.PlaySound("bounce.wav", winsound.SND_ASYNC)

    elif pelota.xcor() > 390:
        pelota.goto(0,0)
        pelota.dx *= -1
        marcador_a += 1
        pen.clear()
        pen.write("Jugador A : {}  Jugador B : {}".format(marcador_a, marcador_b), align="center", font=("Courier", 24, "normal"))

    elif pelota.xcor() < -390:
        pelota.goto(0,0)
        pelota.dx *= -1
        marcador_b += 1
        pen.clear()
        pen.write("Jugador A : {}  Jugador B : {}".format(marcador_a, marcador_b), align="center", font=("Courier", 24, "normal"))

    ## Paddle and Ball Colision
    if (pelota.xcor() > 340 and pelota.xcor() < 350) and (pelota.ycor() < paleta_b.ycor() + 45 and pelota.ycor() > paleta_b.ycor() - 45):
        pelota.setx(340)
        pelota.dx *= -1
        winsound.PlaySound("bounce.wav", winsound.SND_ASYNC)

    if (pelota.xcor() < -340 and pelota.xcor() > -350) and (pelota.ycor() < paleta_a.ycor() + 45 and pelota.ycor() > paleta_a.ycor() - 45):
        pelota.setx(-340)
        pelota.dx *= -1
        winsound.PlaySound("bounce.wav", winsound.SND_ASYNC)